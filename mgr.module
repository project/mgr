<?php

/**
 * Implements hook_permission().
 */
function mgr_permission() {
    return array(
        'access managed content' => array(
            'title' => t('Access managed content'),
            'description' => t('Allow administrators to access the managed content pages.'),
        ),
    );
}

/**
 * Implements hook_menu().
 */
function mgr_menu() {

    $items['admin/mgr'] = array(
        'title' => 'Mgr',
        'page callback' => 'mgr_description',
        'access arguments' => array('access managed content'),
        'expanded' => TRUE,
    );

    $items['admin/mgr/manage'] = array(
        'title' => 'Mgr',
        'page callback' => 'mgr_description',
        'access callback' => TRUE,
        'expanded' => TRUE,
    );

    $nodetypes = node_type_get_names();
    foreach ($nodetypes as $nodetypeid => $nodetypename) {
        $items['admin/mgr/manage/' . $nodetypeid] = array(
            'type' => MENU_LOCAL_TASK,
            'page callback' => 'mgr_managetype',
            'title' => 'Manage ' . $nodetypename . ' Content',
            'page arguments' => array(3),
            'access arguments' => array('access managed content'),
            'expanded' => TRUE,
        );
    }

    return $items;
}


/**
 * Describes the module.
 */
function mgr_description() {
    return array(
        '#markup' =>
            t('<p>The mgr provides an approach to Drupal site building and publishing that is Content-type first.</p>'),
    );
}

/**
 * Mgr Quicktab Content
 */
function mgr_managetype($nodetypeid) {

    $settings = array(
        'ajax' => 1,
        'hide_if_empty' => 1,
        'default_tab' => 4,
        'title' => 'Mgr',
        'renderer' => 'bootstrap_tabs',
        'style' => 'Navlist',
        'options' => array(
            'tabstyle' => 'tabs',
            'tabposition' => 'basic',
            'tabeffects' => array(
                'fade' => 'fade',
            ),
        ),
    );

    $mgrtabs = array(
        0 => array(
            'path' => 'admin/structure/types/manage/' . $nodetypeid . '/',
            'use_title' => TRUE,
            'title' => 'Content Config',
            'weight' => '-100',
            'type' => 'callback',
        ),
        1 => array(
            'path' => 'admin/structure/types/manage/' . $nodetypeid . '/fields',
            'use_title' => TRUE,
            'title' => 'Field Config',
            'weight' => '-99',
            'type' => 'callback',
        ),
        2 => array(
            'path' => 'admin/structure/types/manage/' . $nodetypeid . '/display',
            'use_title' => TRUE,
            'title' => 'Display Config',
            'weight' => '-98',
            'type' => 'callback',
        ),
        3 => array(
            'path' => 'node/add/' . $nodetypeid,
            'use_title' => TRUE,
            'title' => 'Add Content',
            'weight' => '-97',
            'type' => 'callback',
        ),
        4 => array(
            'vid' => 'viewbridgeview',
            'display' => 'page_1',
            'args' => $nodetypeid,
            'use_title' => TRUE,
            'title' => 'List Content',
            'weight' => '-96',
            'type' => 'view',
        ),
        5 => array(
            'vid' => 'viewbridgeview',
            'display' => 'page_2',
            'args' => $nodetypeid,
            'use_title' => TRUE,
            'title' => 'View Content',
            'weight' => '-95',
            'type' => 'view',
        ),
    );

    $mgrtabcontent = quicktabs_build_quicktabs('mgrtabs', $settings, $mgrtabs);
    return array('#markup' => render($mgrtabcontent));
}

/**
 * Implements hook_views_api().
 */
function mgr_views_api() {
    return array("api" => "3.0");
}